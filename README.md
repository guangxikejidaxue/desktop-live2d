# desktopLive2d

#### 介绍
桌面版live2d，支持添加更换模型

#### 软件架构
node.js、electron、electron-forge

#### 效果图：
![效果图](https://images.gitee.com/uploads/images/2021/0122/224323_b9c86ff8_4822721.png "4.png")

#### build
npm config set registry https://registry.npm.taobao.org  
可能需要的其他操作：  
npm install -g electron-forge  
npm install -g electron-prebuilt  
如果出错或者出现什么权限拒绝的问题，重新安装：  
npx @electron-forge/cli import  
npm i --save-dev electron  
npm install -g electron-prebuilt  
npm install -g electron-forge   

#### run
npm run test  

#### package
npx @electron-forge/cli import  
npm i --save-dev electron  
npm install -g electron-prebuilt  
npm install -g electron-forge  
如果前面四步执行过就不需要执行了，如果出现electron缺失啥的，重新npm i --save-dev electron。总之感觉node的坑真多  
最后：  
npm run make  

#### 使用说明
你可以动态添加模型（该操作需要重启程序生效），模型的话，百度 "live2d模型下载" ,就会出现一大堆，但是要注意很多live2d模型是有版权的，不能商用。当然，模型可以自制。   
你需要将模型复制到/src/live2d/model文件夹下，像这样：  
![添加模型](https://images.gitee.com/uploads/images/2021/0111/094610_1593aa2e_4822721.png "屏幕截图.png")
除此之外，你还需要更改/src/live2d/modelList.json文件，把新加的模型的json文件路径添加到json文件里面，像这样：  
![更改modelList.json文件](https://images.gitee.com/uploads/images/2021/0111/094842_866e2e29_4822721.png "屏幕截图.png")

你可以给模型设置背景图片，如果不设置也不会有什么影响。无背景图片的模型将不显示背景图片，背景图片可以这样子加：   

```javascript
{
  "modelList": [
    {
      "modelPath": "./live2d/model/aa12_2403/aa12_2403.model.json",
      "bgUrl":"./bg/live2dBg1.gif"
    },    
    {
      "modelPath": "./live2d/model/Pio/animal-costume.json",
      "bgUrl":"./bg/live2dBg2.gif"
    }
  ]
}
```
##### 背景图片
![背景图片](https://images.gitee.com/uploads/images/2021/0122/224700_daa356c1_4822721.png "0.png")

说明：一般模型的json文件位于模型文件的根路径，像这样     

![模型的json文件](https://images.gitee.com/uploads/images/2021/0111/101225_6c6d036c_4822721.png "屏幕截图.png")

你可以在这里切换模型：  

![切换模型](https://images.gitee.com/uploads/images/2021/0111/095010_4b65d2cd_4822721.png "屏幕截图.png")

####    拖拽主界面：  

只有一个地方可以触发拖拽，那就是这里：  

![拖拽程序](https://images.gitee.com/uploads/images/2021/0111/095114_22bb8e23_4822721.png "屏幕截图.png")

####    系统托盘：  

![系统托盘](https://images.gitee.com/uploads/images/2021/0122/224422_bc351347_4822721.png "2.png")

#### 注意事项  
   该程序主要是针对win平台开发，兼容win7，其他mac和linux平台兼容性没试过。    